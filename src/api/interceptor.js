import axios from 'axios'
import config from "../config";
import {eBus} from '../utils/eventbus';
const token = localStorage.getItem('access_token');
axios.defaults.baseURL = config.apiUrl;
axios.defaults.headers = {
  Authorization: "Bearer " + token
};
export default class AxiosInterceptor {
  // Private constructor
  constructor(baseUrl) {
    this.baseUrl = baseUrl || config.apiUrl;
    return this.init();
  }

  init() {
    let myAxios = axios.create({
      baseURL: this.baseUrl
    });
    myAxios.interceptors.response.use((response) => {
      return response
    }, function (error) {
      if (error.response && error.response.hasOwnProperty('status') && error.response.status === 401) {
        eBus.$emit('unauthorized');
      } else if (error.response && error.response.hasOwnProperty('status') && error.response.status === 403) {
        eBus.$emit('403', error.response.data ? error.response.data.error.message : null);
        return Promise.reject('');
      }
      return Promise.reject(error);
    });

    return myAxios;

  }
}

