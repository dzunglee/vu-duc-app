import api from  '../api';
export default {
    data(){
        return {
            profile: {}
        }
    },
    methods: {
        logout: function () {
            this.$bvModal.msgBoxConfirm('Bạn muốn đăng xuất ngay?', {
                title: '',
                size: 'sm',
                buttonSize: 'sm',
                okVariant: 'danger',
                okTitle: 'Tiếp tục',
                cancelTitle: 'Quay lại',
                footerClass: 'p-2',
                hideHeaderClose: false,
                centered: true
            })
                .then(value => {
                    if (value){
                        localStorage.setItem('access_token', null);
                        this.$router.push('/login');
                    }
                })
                .catch(res => {
                    console.log(res.message)
                });
        },
        getProfile: function () {
            api.getProfile().then(res => {
                if (res.data.success){
                    this.profile = res.data.data;
                }
            }).catch(err => {
                console.log(err);
            });
        }
    },

    mounted() {
        let token = localStorage.getItem('access_token');
        if (!token || token === 'null'){
            this.$router.push('/login')
        }
        this.getProfile();
    }
}