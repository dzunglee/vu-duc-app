import moment from "moment";

const imageExtention = ["image/gif", "image/jpeg", "image/png"];

export default {
  formatDateTime(date) {
    if (date) {
      return moment(date, "YYYY-MM-DD HH:mm:ss").format("DD-MM-YYYY HH:mm:ss");
    } else {
      return moment().format("DD-MM-YYYY HH:mm:ss");
    }
  },
  formatServerDateTime(date) {
    if (date) {
      return moment(date, "DD-MM-YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss");
    } else {
      return moment().format("YYYY-MM-DD HH:mm:ss");
    }
  },
  formatDate(date) {
    if (date) {
      return moment(date, "YYYY-MM-DD").format("DD-MM-YYYY");
    } else {
      return moment().format("DD-MM-YYYY");
    }
  },
  formatServerDate(date) {
    if (date) {
      return moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
    } else {
      return moment().format("YYYY-MM-DD");
    }
  },
  urlQueryString(data, allowNULL = true) {
    return Object.keys(data)
      .map(function(key) {
        if (!allowNULL && !data[key]) {
          return false;
        }
        return [key, data[key]].map(encodeURIComponent).join("=");
      })
      .filter(val => val)
      .join("&");
  },

  /**
   * Only pick that is matched on object vs template
   * @param object
   * @param template
   */
  pick(object, template = array) {
    return _.pick(object, template);
  },

  /**
   * Check whether object is matched or not
   * @param object
   * @param template
   * @returns {object|null}
   */
  mandatory(object, template = array) {
    let match = _.pick(object, template);
    if (Object.keys(match) && Object.keys(match).length == template.length) {
      return object;
    } else {
      return null;
    }
  },

  /**
   * Generate a Desciprtion for Beli > Listrik
   * @param danon
   * @returns {string}
   */
  genListrikDescription(danon) {
    return "Token Listrik" + " " + new Intl.NumberFormat().format(danon);
  },

  /**
   * Generate a Desciprtion for Beli > Pulsa
   * @param danon
   * @returns {string}
   */
  genPulsaDescription(cellular_name, danon) {
    return (
      "Pulsa" +
      " " +
      cellular_name +
      " " +
      new Intl.NumberFormat().format(danon)
    );
  },

  formatPhone: function(phone) {
    console.warn("Deprecation warning: Should use this.formatPhone");
    phone = phone.replace(/(\d{4})(\d{3})(\d{4})/, "($1)-$2-$3");
    return phone;
  },
  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },
  isImage: function(file) {
    if (file && file.type && imageExtention.indexOf(file.type) > -1)
      return true;
    return false;
  },
  setCookie(name, value, days) {
    let expires = "";
    if (days) {
      let date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
  },
  getCookie(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(";");
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === " ") c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  },
  validUserName(name) {
    console.warn("Deprecation warning: Should use this.validUserName");
    let regex = /^[+][0-9]{6,}$/;
    return regex.test(name);
  },
  validPin(pin) {
    console.warn("Deprecation warning: Should use this.validPin");
    let regex = /^[0-9]{6,6}$/;
    return regex.test(pin);
  },
  fmtMSS(s) {
    console.warn("Deprecation warning: Should use this.fmtMSS");
    return (s - (s %= 60)) / 60 + (9 < s ? ":" : ":0") + s;
  },
  showAmount(value) {
    console.warn("Deprecation warning: Should use this.showAmount");
    return value ? parseInt(value).toLocaleString("en-US") : 0;
  },
  getStatus(configData, type, field) {
    console.warn("Deprecation warning: Should use this.getStatus");
    return configData[type] && configData[type][field]
      ? configData[type][field]
      : "";
  },

  getErrorMessageOrDefault(error, text) {
    console.log(error.response.data.error.code);
    if (
      error.response &&
      error.response.data &&
      error.response.data.error &&
      error.response.data.error.code === 400 &&
      error.response.data.error.message
    ) {
      return error.response.data.error.message;
    }

    return text;
  }
};
